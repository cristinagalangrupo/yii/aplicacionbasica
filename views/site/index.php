<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'primer ejemplo de Yii';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>¡Felicidades!</h1>

        <p class="lead">Desarrollado Cristina Galán.</p>

        
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>API de Yii</h2>

                <p>Todas las clases de Yii.</p>

                <p><a class="btn btn-default" href="https://www.yiiframework.com/doc/api/2.0">API Yii 2</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Manual Yii 2</h2>

                <p>Manual de Clases de  Yii2.</p>
               <p> <?= Html::a('Guía de Yii', 'https://www.yiiframework.com/doc/guide/2.0/es',['class' => 'btn btn-default']) ?></p>
               
            </div>
            
        </div>

    </div>
</div>
