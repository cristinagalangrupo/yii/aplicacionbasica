<?php
use yii\helpers\Html;

?>

    
    <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <?= Html::img('@web/imgs/1.jpg', ['alt' => 'My logo']) ?>
      
      <div class="caption">
        <h3>Foto 1</h3>
        
      </div>
    </div>
  </div>
         <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <?= Html::img('@web/imgs/2.jpg', ['alt' => 'My logo']) ?>
      
      <div class="caption">
        <h3>Foto 2</h3>
        
      </div>
    </div>
  </div>
         <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <?= Html::img('@web/imgs/3.jpg', ['alt' => 'My logo']) ?>
      
      <div class="caption">
        <h3>Foto 3</h3>
        
      </div>
    </div>
  </div>
        
</div>