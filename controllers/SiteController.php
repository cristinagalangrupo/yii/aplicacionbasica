<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionEstamos()
    {
        return $this->render('dondeestamos');
    }
    public function actionConocenos()
    {
        return $this->render('conocenos');
    }
     public function actionContacto()
    {
         //var_dump(Yii::$app->request->get('nombre'));
         //var_dump($_REQUEST);
         $datos=Yii::$app->request->get();
         $vista="contacto";
         $longitud=0;
         
         /**
          * conocer si he pulsado el botón enviar y por lo tanto 
          * me llegan datos
          */
    if(isset($datos['boton'])){
            $longitud= strlen($datos['nombre']);
            $vista="resultado";
                 
         }
         return $this->render($vista,[
             "titulo"=>"Introduce tus datos",
             "longitud"=>$longitud,
             ]);
    }
    

   

}
